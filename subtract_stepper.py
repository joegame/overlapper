from overlap import overlap_fast
import cv2 as cv
import numpy as np
# from numpy import nditer

class SubtractStepper:

    def __init__(self, filen, offset, comp):
        self.offset = offset
        self.capture = cv.VideoCapture(cv.samples.findFileOrKeep(filen))

        if not self.capture.isOpened():
            print('Unable to open: ' + args.input)
            exit(0)

        self.capture.set(cv.CAP_PROP_POS_MSEC, offset)
        self.backSub = cv.createBackgroundSubtractorKNN()

        self.frame_width = int(self.capture.get(cv.CAP_PROP_FRAME_WIDTH)) -1
        self.frame_height = int(self.capture.get( cv.CAP_PROP_FRAME_HEIGHT)) -1
        # self.vfunc = np.vectorize(comparePixels, excluded=['comp'])
        # self.grayfunc = np.vectorize(gray2RGB, otypes=[np.ndarray])
        self.comp = comp

        super().__init__()

    def step(self, wframe):
        ret, frame = self.capture.read()
        if frame is None: return frame
        fgmask = self.backSub.apply(frame)
        # fgMask = np.stack((fgMask,)*3, axis=-1)

        # wframe = self.vfunc(fgMask,frame,wframe, 100)

        # for x in range(self.frame_width):
        #     for y in range(self.frame_height):
        #         if fgMask.item((y,x)) > self.comp:
        #             # print(frame.shape)
        #             fr = frame[y][x]
        #             # wframe[x][y] = fr
        #             wframe.itemset((y,x,0),fr[0])
        #             wframe.itemset((y,x,1),fr[1])
        #             wframe.itemset((y,x,2),fr[2])

        # return wframe
        # print(fgmask)
        wframe = overlap_fast(self.comp, wframe, frame, fgmask)
        return np.asarray(wframe)

def comparePixels(mask,new_frame,old_frame,comp):
    if mask > comp:
        return new_frame
    else:
        return old_frame

def gray2RGB(input):
    return [input,input,input]
