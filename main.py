from __future__ import print_function
import cv2 as cv
import argparse
from subtract_stepper import SubtractStepper


# if args.algo == 'MOG2':
# backSub = cv.createBackgroundSubtractorMOG2()
# else:
backSub = cv.createBackgroundSubtractorKNN()

capture = cv.VideoCapture(cv.samples.findFileOrKeep("birds_full_sm.mp4"))
if not capture.isOpened():
    print('Unable to open: ' + args.input)
    exit(0)

# Obtain frame size information using get() method
frame_width = int(capture.get(cv.CAP_PROP_FRAME_WIDTH))
frame_height = int(capture.get( cv.CAP_PROP_FRAME_HEIGHT))
frame_size = (frame_width,frame_height)
fourcc = cv.VideoWriter_fourcc(*"mp4v")
fps = capture.get(cv.CAP_PROP_FPS)

writer = cv.VideoWriter("test.mp4",fourcc, fps, frame_size)


steppers = []
for i in range(50):
    steppers.append(SubtractStepper( "birds_full_sm.mp4", (i+1)*50000, 100))

def getTime(cap):
     return cap.get(cv.CAP_PROP_POS_FRAMES)/cap.get(cv.CAP_PROP_FRAME_COUNT)
    # return (cap.get(cv.CAP_PROP_FRAME_COUNT)/cap.get(cv.CAP_PROP_FPS))*cap.get(cv.CAP_PROP_POS_AVI_RATIO)

while True:
    print(getTime(capture))
    ret, frame = capture.read()
    if capture.get(cv.CAP_PROP_POS_MSEC) > 50000:
        writer.release()
        break
    if frame is None:
        writer.release()
        break
    for i in steppers:
        frame = i.step(frame)
    writer.write(frame)
