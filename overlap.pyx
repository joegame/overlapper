cimport cython
from cython.parallel import prange


@cython.boundscheck(False)
cpdef unsigned char [:, :, :] overlap_fast(int T,
                                       unsigned char [:, :, :] baseimage,
                                       unsigned char [:, :, :] image,
                                       unsigned char [:, :] mask):
# set the variable extension types
    cdef int x, y, w, h

    # grab the image dimensions
    h = baseimage.shape[0]
    w = baseimage.shape[1]

    # loop over the image
    with nogil:
        for y in prange(0, h, nogil=true):
                for x in prange(0, w, nogil=true):
                    if mask[y, x] > T:
                        # print(baseimage[y,x])
                        baseimage[y, x] = image[y, x]

    # return the thresholded image
    # print( baseimage[44,44] )
    return baseimage
