#!/usr/bin/env python3

from setuptools import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

setup(
    name='overlap',
    zip_safe=False,
    ext_modules=[
        Extension('overlap',
                  sources=['overlap.pyx'],
                  extra_compile_args=['-fopenmp'],
                  extra_link_args=['-fopenmp']
                  )
    ],
    cmdclass = {'build_ext': build_ext}
)
